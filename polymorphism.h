/*
File	: polymorphism.h
===============================================================
<< History Revision >>
Date			Author		Contents
2020-04-30		Pipit L.	Initial released
2020-04-30		Pipit L.	Added following class,
							 - Employee{}
							 - Manager{}
===============================================================
*/

#ifndef POLYMORPHISM_H
#define POLYMORPHISM_H

using namespace std;

#define _PI	3.1415926535897
#define _DEFAULT_RATE_BONUS	10.5

class Shape2D
{
	public:
		virtual double getArea(void) = 0;
		double calCostOfLand(Shape2D *pShape2D);
};

class Rectangle :public Shape2D
{
	private:
		double width;
		double height;
	public:
		Rectangle(double _width_, double _height_);
		double getArea(void);
};

class Circle :public Shape2D
{
	private:
		double radius;
	public:
		Circle(double _radius_);
		double getArea(void);
};

class Triangle :public Shape2D
{
	private:
		double base;
		double height;
	public:
		Triangle(double _base_, double _height_);
		double getArea();
};



class Employee
{
private:
	int id;
	string names;
	double salary;
	double yearOfWork;
	int jobGrade;
public:
	Employee(void)
	{

	}
	void setId(int _id_);
	int getId(void);
	void setName(string _name_);
	string getName(void);
	void setSalary(double _salary);
	double getSalary(void);
	void setYearOfWork(double _yearOfWork_);
	double getYearOfWork(void);
	void setJobGrade(int _jobGrade_);
	int getJobGrade(void);
	virtual double getRateBonus(void) = 0;
	double calBonus(Employee *pEmployee);
};

class Manager :public Employee
{
private:
	string parkNo;
public:
	Manager(void) { }
	void setParkNo(string _parkNo_);
	string getParkNo(void);
	double getRateBonus(void);
};

#endif // !POLYMORPHISM_H
