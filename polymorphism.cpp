/*
File	: polymorphism.cpp
===============================================================
<< History Revision >>
Date			Author		Contents
2020-04-30		Pipit L.	Initial released
2020-04-30		Pipit L.	Added following class,
							 - Employee{}
							 - Manager{}
===============================================================
*/

#include <iostream>
#include "polymorphism.h"

using namespace std;

double Shape2D::calCostOfLand(Shape2D *pShape2D)
{
	return pShape2D->getArea() * 100.0;
}

Rectangle::Rectangle(double _width_, double _height_)
{
	width = _width_;
	height = _height_;
}
double Rectangle::getArea(void)
{
	return width * height;
}

Circle::Circle(double _radius_)
{
	radius = _radius_;
}
double Circle::getArea(void)
{
	return _PI * radius * radius;
}

Triangle::Triangle(double _base_, double _height_)
{
	base = _base_;
	height = _height_;
}
double Triangle::getArea()
{
	return 0.5 * base * height;
}

void Employee::setId(int _id_)
{
	id = _id_;
}

int Employee::getId(void)
{
	return id;
}

void Employee::setName(string _name_)
{
	names = _name_;
}

string Employee::getName(void)
{
	return names;
}

void Employee::setSalary(double _salary)
{
	salary = _salary;
}

double Employee::getSalary(void)
{
	return salary;
}

void Employee::setYearOfWork(double _yearOfWork_)
{
	yearOfWork = _yearOfWork_;
}

double Employee::getYearOfWork(void)
{
	return yearOfWork;
}

void Employee::setJobGrade(int _jobGrade_)
{
	jobGrade = _jobGrade_;
}

int Employee::getJobGrade(void)
{
	return jobGrade;
}

double Employee::calBonus(Employee *pEmployee)
{
	double ratio;

	if (yearOfWork >= 1)
	{
		ratio = pEmployee->getRateBonus() * salary;
	}
	else
	{
		ratio = pEmployee->getRateBonus() * yearOfWork * salary;
	}

	return ratio;
}

void Manager::setParkNo(string _parkNo_)
{
	parkNo = _parkNo_;
}

string Manager::getParkNo(void)
{
	return parkNo;
}

double Manager::getRateBonus(void)
{
	return _DEFAULT_RATE_BONUS;
}