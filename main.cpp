/*
File	: main.cpp
===============================================================
<< History Revision >>
Date			Author		Contents
2020-04-30		Pipit L.	Initial released

2020-04-30		Pipit L.	Added following class,
							 - Employee{}
							 - Manager{}
2020-04-30		Pipit L.	Merge develop branch and master to release.
===============================================================
*/

#define 5

#include <iostream>
#include <string>
#include <list>
#include "polymorphism.h"

using namespace std;

int main(void)
{


	system("pause");
	return 0;
}